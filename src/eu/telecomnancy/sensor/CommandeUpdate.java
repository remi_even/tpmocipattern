package eu.telecomnancy.sensor;

public class CommandeUpdate extends ISensorCommand {

	public CommandeUpdate(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void execute() {
		try {
			receiver.update();
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}

}
