package eu.telecomnancy.sensor;

import java.time.LocalDateTime;
import java.util.Observable;

public class ProxySensor extends Observable implements ISensor {

	private ISensor sensor;

	public ProxySensor(ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void on() {
		sensor.on();
		System.out.println(LocalDateTime.now() + " : sensor activated");
		setChanged();
		notifyObservers();
	}

	@Override
	public void off() {
		sensor.off();
		System.out.println(LocalDateTime.now() + " : sensor deactivated");
		setChanged();
		notifyObservers();
	}

	@Override
	public boolean getStatus() {
		boolean result = sensor.getStatus();
		System.out.println(LocalDateTime.now() + " : sensor status is " + (result ? "on" : "off"));
		return result;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		try {
			sensor.update();
			System.out.println(LocalDateTime.now() + " : successful update of the sensor");
			setChanged();
			notifyObservers();
		} catch (SensorNotActivatedException e) {
			System.out.println(LocalDateTime.now() + " : can't update of the sensor");
			throw e;
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		try {
			double result = sensor.getValue();
			System.out.println(LocalDateTime.now() + " : current measure of the sensor is " + result);
			return result;
		} catch (SensorNotActivatedException e) {
			System.out.println(LocalDateTime.now() + " : can't get the measure of the sensor");
			throw e;
		}
	}

}
