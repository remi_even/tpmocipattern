package eu.telecomnancy.sensor;

public abstract class ISensorCommand {

	protected ISensor receiver;
	
	public ISensorCommand(ISensor sensor) {
		this.receiver = sensor;
	}
	
	public abstract void execute();
}
