package eu.telecomnancy.sensor;

public class CommandeOff extends ISensorCommand {

	public CommandeOff(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void execute() {
		receiver.off();
	}

}
