package eu.telecomnancy.sensor;

public class CommandInvoker {
	
	public void execute(ISensorCommand command) {
		command.execute();
	}

}
