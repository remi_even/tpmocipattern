package eu.telecomnancy.sensor;

/**
 * 
 * @author R�mi Even
 *
 */
import java.util.Observable;
import java.util.Random;

public class StateTemperatureSensor extends Observable implements ISensor {
    
	private StateTemperatureSensorState state;
	
	public StateTemperatureSensor() {
		state = new StateTemperatureSensorStateOff();
	}

    @Override
    public void on() {
        state = new StateTemperatureSensorStateOn();
        setChanged();
        notifyObservers();
    }

    @Override
    public void off() {
        state = new StateTemperatureSensorStateOff();
        setChanged();
        notifyObservers();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
       state.update();
       setChanged();
       notifyObservers();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	return state.getValue();
    }

}
