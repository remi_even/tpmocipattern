package eu.telecomnancy.sensor;

public class CommandeOn extends ISensorCommand {

	public CommandeOn(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void execute() {
		receiver.on();
	}

}
