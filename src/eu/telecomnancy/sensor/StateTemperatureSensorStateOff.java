package eu.telecomnancy.sensor;

/**
 * 
 * @author R�mi Even
 *
 */
public class StateTemperatureSensorStateOff implements StateTemperatureSensorState {
	
	public StateTemperatureSensorStateOff() {}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public boolean getStatus() {
		return false;
	}

}
