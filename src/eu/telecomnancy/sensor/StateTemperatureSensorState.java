package eu.telecomnancy.sensor;

/**
 * 
 * @author R�mi Even
 *
 */
public interface StateTemperatureSensorState {
	public double getValue() throws SensorNotActivatedException;
	public void update() throws SensorNotActivatedException;
	public boolean getStatus();
}
