package eu.telecomnancy.sensor;

import java.util.Observable;

public abstract class ISensorConverter extends Observable implements ISensor {

	protected ISensor sensor;

	public ISensorConverter(ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void on() {
		sensor.on();
		setChanged();
		notifyObservers();
	}

	@Override
	public void off() {
		sensor.off();
		setChanged();
		notifyObservers();
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
		setChanged();
		notifyObservers();
	}

	@Override
	public abstract double getValue() throws SensorNotActivatedException;
	
	public abstract String getUnit();

}
