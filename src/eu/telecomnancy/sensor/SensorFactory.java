package eu.telecomnancy.sensor;

public class SensorFactory {
	
	public static TemperatureSensor makeTemperatureSensor() {
		return new TemperatureSensor();
	}
	
	public static StateTemperatureSensor makeStateTemperatureSensor() {
		return new StateTemperatureSensor();
	}
	
	public static LegacyTemperatureSensorAdapter makeLegacyTemperatureSensorAdapter() {
		return new LegacyTemperatureSensorAdapter(new LegacyTemperatureSensor());
	}
	
	public static ISensor makeISensor(String className) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
			return (ISensor) Class.forName(className).newInstance();
	}

}
