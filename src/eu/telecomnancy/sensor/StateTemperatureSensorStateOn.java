package eu.telecomnancy.sensor;

import java.util.Random;

/**
 * 
 * @author R�mi Even
 *
 */
public class StateTemperatureSensorStateOn implements StateTemperatureSensorState {
	
	private double value;
	
	public StateTemperatureSensorStateOn() {
		try {
			update();
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public boolean getStatus() {
		return true;
	}

}
