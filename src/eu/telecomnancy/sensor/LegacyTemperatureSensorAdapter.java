package eu.telecomnancy.sensor;

import java.util.Observable;

/**
 * 
 * @author R�mi Even
 *
 */
public class LegacyTemperatureSensorAdapter extends Observable implements ISensor {
	
	private LegacyTemperatureSensor sensor;
	private double lastValue;
	
	public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void on() {
		if (!sensor.getStatus()) {
			sensor.onOff();
			lastValue = sensor.getTemperature();
	        setChanged();
	        notifyObservers();
		}
	}

	@Override
	public void off() {
		if (sensor.getStatus()) {
			sensor.onOff();
	        setChanged();
	        notifyObservers();
		}
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (sensor.getStatus()) {
			lastValue = sensor.getTemperature();
			this.setChanged();
			this.notifyObservers();
		} else {
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new value.");
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (sensor.getStatus())
			return lastValue;
		else
			throw new SensorNotActivatedException("Sensor must be activated before getting its value");
	}
}
