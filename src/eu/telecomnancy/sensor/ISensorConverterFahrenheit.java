package eu.telecomnancy.sensor;

public class ISensorConverterFahrenheit extends ISensorConverter {

	public ISensorConverterFahrenheit(ISensor sensor) {
		super(sensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return (1.8 * sensor.getValue() + 32);
	}
	
	@Override
	public String getUnit() {
		return "�F";
	}

}
