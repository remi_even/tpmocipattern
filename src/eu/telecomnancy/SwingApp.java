package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ISensorConverterFahrenheit;
import eu.telecomnancy.sensor.ProxySensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.sensor.StateTemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
    	ISensor sensor;
		try {
			sensor = SensorFactory.makeISensor("eu.telecomnancy.sensor.StateTemperatureSensor");
	        new MainWindow(sensor);
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			e.printStackTrace();
		}
    }

}
