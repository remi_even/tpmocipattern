package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.sensor.CommandInvoker;
import eu.telecomnancy.sensor.CommandeOff;
import eu.telecomnancy.sensor.CommandeOn;
import eu.telecomnancy.sensor.CommandeUpdate;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ISensorCommand;
import eu.telecomnancy.sensor.ISensorConverter;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorView extends JPanel implements Observer {
    private ISensor sensor;
    private CommandInvoker commandInvoker = new CommandInvoker();

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) {
        this.sensor = c;
        ((Observable) sensor).addObserver(this);

        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);

        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ISensorCommand command = new CommandeOn(sensor);
                commandInvoker.execute(command);
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	ISensorCommand command = new CommandeOff(sensor);
                commandInvoker.execute(command);
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	ISensorCommand command = new CommandeUpdate(sensor);
                commandInvoker.execute(command);
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    /**
     * @author R�mi Even
     */
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0 instanceof ISensor) {
			ISensor sensor = (ISensor) arg0;
			try {
				if (sensor.getStatus()) {
					String unit;
					if (sensor instanceof ISensorConverter)
						unit = ((ISensorConverter) sensor).getUnit();
					else
						unit = "�C";
					value.setText(sensor.getValue() + " " + unit);
				} else {
					value.setText("N/A °C");
				}
			} catch (SensorNotActivatedException e) {
				e.printStackTrace();
			}
		}
	}
}
