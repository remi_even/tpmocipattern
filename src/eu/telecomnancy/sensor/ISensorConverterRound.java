package eu.telecomnancy.sensor;

public class ISensorConverterRound extends ISensorConverter {

	public ISensorConverterRound(ISensor sensor) {
		super(sensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return Math.round(sensor.getValue() * 1000) / 1000;
	}
	
	@Override
	public String getUnit() {
		return "�C";
	}

}
